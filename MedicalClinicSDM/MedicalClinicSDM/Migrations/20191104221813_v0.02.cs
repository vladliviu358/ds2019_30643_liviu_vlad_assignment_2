﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MedicalClinicSDM.Migrations
{
    public partial class v002 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AccountType",
                table: "Pacient",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "AccountType",
                table: "Medic",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "AccountType",
                table: "Caregiver",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountType",
                table: "Pacient");

            migrationBuilder.DropColumn(
                name: "AccountType",
                table: "Medic");

            migrationBuilder.DropColumn(
                name: "AccountType",
                table: "Caregiver");
        }
    }
}
