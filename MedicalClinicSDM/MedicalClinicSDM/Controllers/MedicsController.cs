﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MedicalClinicSDM.Data;
using MedicalClinicSDM.Models;
using Microsoft.AspNetCore.Http;

namespace MedicalClinicSDM.Controllers
{
    public class MedicsController : Controller
    {
        private readonly MedicalClinicSDMContext _context;

        public MedicsController(MedicalClinicSDMContext context)
        {
            _context = context;
        }

        // GET: Medics
        public async Task<IActionResult> Index()
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            return View(await _context.Medic.ToListAsync());
        }

        public IActionResult AccessDet()
        {
            return RedirectToAction("Details", "Medics", new { id = HttpContext.Session.GetInt32("UserID") });
        }

        // GET: Medics/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var medic = await _context.Medic
                .FirstOrDefaultAsync(m => m.ID == id);
            if (medic == null)
            {
                return NotFound();
            }

            return View(medic);
        }

        // GET: Medics/Create
        public IActionResult Create()
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            return View();
        }

        // POST: Medics/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Username,Password,Name,BirthDate,Gender,Address,AccountType")] Medic medic)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            medic.AccountType = 2;

            if (ModelState.IsValid)
            {
                _context.Add(medic);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(medic);
        }

        // GET: Medics/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var medic = await _context.Medic.FindAsync(id);
            if (medic == null)
            {
                return NotFound();
            }
            return View(medic);
        }

        // POST: Medics/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Username,Password,Name,BirthDate,Gender,Address")] Medic medic)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id != medic.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(medic);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MedicExists(medic.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(medic);
        }

        // GET: Medics/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var medic = await _context.Medic
                .FirstOrDefaultAsync(m => m.ID == id);
            if (medic == null)
            {
                return NotFound();
            }

            return View(medic);
        }

        // POST: Medics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            var medic = await _context.Medic.FindAsync(id);
            _context.Medic.Remove(medic);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MedicExists(int id)
        {
            return _context.Medic.Any(e => e.ID == id);
        }
    }
}
