﻿using MedicalClinicSDM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalClinicSDM.Data
{
    public class MedicalClinicSDMContext : DbContext
    {
        public MedicalClinicSDMContext(DbContextOptions<MedicalClinicSDMContext> options)
            : base(options)
        {


        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Pacient>()
                .HasOne<Medic>(s => s.Medic)
                .WithMany(g => g.Pacients)
                .HasForeignKey(s => s.MedicID);

            modelBuilder.Entity<Notification>()
                .HasOne<Pacient>(p => p.Pacient)
                .WithMany(n => n.Notifications)
                .HasForeignKey(p => p.PacientID);

            modelBuilder.Entity<CaregiverPacient>().HasKey(sc => new { sc.CaregiverID, sc.PacientID });

            modelBuilder.Entity<CaregiverPacient>()
                .HasOne<Caregiver>(sc => sc.Caregiver)
                .WithMany(s => s.CaregiverPacients)
                .HasForeignKey(sc => sc.CaregiverID);

            modelBuilder.Entity<CaregiverPacient>()
                .HasOne<Pacient>(sc => sc.Pacient)
                .WithMany(s => s.CaregiverPacients)
                .HasForeignKey(sc => sc.PacientID);

            modelBuilder.Entity<MedicationPacient>().HasKey(mp => new { mp.MedicationID, mp.PacientID });

            modelBuilder.Entity<MedicationPacient>()
                .HasOne<Medication>(mp => mp.Medication)
                .WithMany(s => s.MedicationPacients)
                .HasForeignKey(mp => mp.MedicationID);

            modelBuilder.Entity<MedicationPacient>()
                .HasOne<Pacient>(mp => mp.Pacient)
                .WithMany(s => s.MedicationPacients)
                .HasForeignKey(mp => mp.PacientID);


        }

        public DbSet<global::MedicalClinicSDM.Models.Pacient> Pacient { get; set; }

        public DbSet<global::MedicalClinicSDM.Models.Medic> Medic { get; set; }

        public DbSet<global::MedicalClinicSDM.Models.Caregiver> Caregiver { get; set; }

        public DbSet<global::MedicalClinicSDM.Models.Medication> Medication { get; set; }

        public DbSet<MedicalClinicSDM.Models.MedicationPacient> MedicationPacient { get; set; }

        public DbSet<MedicalClinicSDM.Models.CaregiverPacient> CaregiverPacient { get; set; }

        public DbSet<MedicalClinicSDM.Models.Notification> Notification { get; set; }
    }
}
